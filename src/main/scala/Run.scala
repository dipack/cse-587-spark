import analyser.{Analyser, Cleaner, Rating}
import org.apache.spark.ml.linalg.{Matrix, Vectors}
import org.apache.spark.ml.stat.Correlation
import org.apache.spark.mllib.evaluation.RankingMetrics
import org.apache.spark.mllib.recommendation.ALS
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Row, SparkSession}
import tweeter.TwitterCollector
import twitter4j.Status

object Run {
  def main(args: Array[String]) = {
    val spark = SparkSession.builder().appName("lab-3").master("local[*]").getOrCreate()
    var ctx = spark.sparkContext
    import spark.implicits._
    val tw = new TwitterCollector
    val analyser = new Analyser
    val batmanResults: List[Rating] = tw.collectTweets("batman").map(x => analyser.analyseText(x.getText))
    val batmanScore = batmanResults.map(_.rating).sum
    val supermanResults: List[Rating] = tw.collectTweets("superman").map(x => analyser.analyseText(x.getText))
    val supermanScore = supermanResults.map(_.rating).sum
    val results = batmanResults ++ supermanResults
    println(f"Printing results for all queries")
    results.foreach(x => println(f"Text: (${x.text}) Score: (${x.rating})"))
    val df = spark.createDataFrame(results.map(x => (x.text, x.rating))).toDF("text", "rating")
  }
}