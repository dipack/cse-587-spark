package analyser

object Cleaner {
  val stopLines: List[String] = scala.io.Source.fromResource("english_stopwords.txt").getLines.map(Cleaner.cleanWord).toList
  val stopWords: List[String] = stopLines.map(Cleaner.cleanWord)

  def cleanWord(word: String): String = {
    word.filter(_.isLetter).toLowerCase
  }

  def cleanTweet(tweetStr: String): String = {
    val words = tweetStr.split("\n").toList.mkString(" ").split(" ")
    words.map(cleanWord).filter(!stopWords.contains(_)).mkString(" ")
  }
}
