package analyser


import analyser.Rating
import org.apache.spark.ml.classification.LogisticRegression
// Word dictionaries sourced from these URLs
// http://ptrckprry.com/course/ssd/data/positive-words.txt
// http://ptrckprry.com/course/ssd/data/negative-words.txt

class Analyser {
  val positiveLines: List[String] = scala.io.Source.fromResource("positive-words.txt").getLines.toList
  val positiveWords: List[String] = positiveLines.map(Cleaner.cleanWord)
  val negativeLines: List[String] = scala.io.Source.fromResource("negative-words.txt").getLines.map(Cleaner.cleanWord).toList
  val negativeWords: List[String] = negativeLines.map(Cleaner.cleanWord)
  val model = new org.apache.spark.ml.classification.LogisticRegression

  def analyseText(text: String): Rating = {
    var score: Long = 0
    val words = Cleaner.cleanTweet(text).split(" ")
    words.foreach(word => {
      if (positiveWords.contains(word)) {
        score += 1
      } else if (negativeWords.contains(word)) {
        score -= 1
      }
    })
    new Rating(words.mkString(" "), score)
  }
}
