package tweeter

import scala.collection.JavaConverters._
import twitter4j.conf.ConfigurationBuilder
import twitter4j.{Query, QueryResult, Status, Twitter, TwitterFactory}

class TwitterCollector {

  def collectTweets(topic: String, num: Integer = 10): List[Status] = {
    val config = new ConfigurationBuilder
    config.setDebugEnabled(true)
      .setOAuthConsumerKey("M1kgGQc23G6kaQyUuQJmwUNgK")
      .setOAuthConsumerSecret("tdYLiqHNeBl5srzHPXDjwDFkJT6H7uuQW2Fe6CMNZ8M83oGBlG")
      .setOAuthAccessToken("1093611197486768128-GlPEQQLoiJOxqZgi9jCWbL8nD1Tiws")
      .setOAuthAccessTokenSecret("BdITbeI2oNKzy5QZX2ZDUj9ceP5VaeLyQRlKPIA2EBpCS")
    val queryStr = f"$topic -filter:retweets AND -filter:replies"
    val twitter: Twitter = new TwitterFactory(config.build()).getInstance
    val query: Query = new Query(queryStr)
    query.setCount(num)
    query.setLang("en")
    val result: QueryResult = twitter.search(query)
    val tweets = result.getTweets
    tweets.asScala.toList
  }
}
