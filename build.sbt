name := "lab-3"

version := "0.1"

scalaVersion := "2.12.8"

val sparkVersion = "2.4.2"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion
)

libraryDependencies ++= Seq(
  "org.twitter4j" % "twitter4j-core" % "4.0.3",
  "org.twitter4j" % "twitter4j-stream" % "4.0.3"
)
